<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>National University | Home</title>
		<link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="custom.css" rel="stylesheet">
	</head>
	<body>
		<div class="navbar ">
			<div class="container-fluid">
				<div class="col-md-2 col-lg-2 logo">
					<img src="images/logo.jpg" alt="National University">
				</div>
				<div class="col-md-8 col-lg-8 banner">
					<img src="images/banner.jpg" alt="National University">
				</div>
				<div class="col-md-2 col-lg-2 logo">
					<img src="images/logo.jpg" alt="National University">
				</div>
			</div>
		</div>
		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		        <li class="active"><a href="#"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>&nbsp;Home <span class="sr-only">(current)</span></a></li>
		        <li><a href="#">Prospective Students</a></li>
		        <li><a href="#">Current Students</a></li>
		        <li><a href="#">Alumni</a></li>
		        <li><a href="#">Faculty Members</a></li>

		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Activities<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">Action</a></li>
		            <li><a href="#">Another action</a></li>
		            <li><a href="#">Something else here</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="#">Separated link</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="#">One more separated link</a></li>
		          </ul>
		        </li>
		      </ul>

		      <ul class="nav navbar-nav navbar-right">
			      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
			      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			   </ul>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#myCarousel" data-slide-to="1"></li>
		    <li data-target="#myCarousel" data-slide-to="2"></li>
		    <li data-target="#myCarousel" data-slide-to="3"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner" role="listbox">
		    <div class="item active">
		      <img src="images/curosol1.jpg" alt="Chania">
		    </div>
		    <div class="item">
		      <img src="images/curosol2.jpg" alt="Chania">
		    </div>
		    <div class="item">
		      <img src="images/curosol3.jpg" alt="Flower">
		    </div>
		    <div class="item">
		      <img src="images/curosol4.jpg" alt="Flower">
		    </div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
		<!-- Main body-->
		<div class="body">
			<div class="col-md-2">
				<div class="list-group">
					<br>
					<br>
					<br>
					<br>
				<!-- side menu start-->
				<nav class="navbar navbar-inverse sidebar" role="navigation">
				    <div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="#">Welcome NU</a>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="#">Home&nbsp;&nbsp;<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>
								<li ><a href="#">Profile &nbsp;<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>
								<li ><a href="#">Messages <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-envelope"></span></a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admission <span class="caret"></a>
									<ul class="dropdown-menu forAnimate" role="menu">
										<li><a href="#">PG admission</a></li>
										<li><a href="#">Graduate admission</a></li>
										<li><a href="#">Something else here</a></li>
										<li class="divider"></li>
										<li><a href="#">Separated link</a></li>
										<li class="divider"></li>
										<li><a href="#">One more separated link</a></li>
									</ul>
								</li>
								<li><a href="#">Research</a></li>
								<li ><a href="#">Research Grant</span></a></li>
								<li ><a href="#">Outreach</a></li>
								<li ><a href="#">Activities</span></a></li>
								<li ><a href="#">Faculty</a></li>
								<li ><a href="#">Officers</a></li>
								<li ><a href="#">Staff</a></li>
								<li ><a href="#">Alumni</a></li>
								<li ><a href="#">News</a></li>
								<li ><a href="#">Calendar Events</span></a></li>
								<li ><a href="#">Public Notices</a></li>
								<li ><a href="#">Fulbright scholarship</a></li>
								<li ><a href="#">Contact Us</a></li>
							</ul>
						</div>
					</div>
				</nav>
				<!-- side menu End-->
				</div>
			</div>
			<div class="col-md-6"><!-- col 6 start-->
				 <div class="header">
					<h3><span class="glyphicon glyphicon-th-large"></span>&nbsp;CS Features</h3></br>
					<div class="well">
						<img src="images/features.png" alt="..." class="img-thumbnail">
						<h4><span class="glyphicon glyphicon-play"></span>&nbsp;Prospects of CSE:</h4>
						<p>This document was prepared as a leaflet for the seminar 'Prospects of CSE' organized by Department of CSE, BUET and CSEBUET 99 Alumni Association. The seminar was held on November 26, 2005 at the BUET Central Auditorium.e</p>
						<p><a class="btn btn-primary btn-sm" href="#" role="button">Learn more >></a></p>
					</div>
				 </div>
				 <div class="header">
					<h3><span class="glyphicon glyphicon-list-alt"></span>&nbsp;Events</h3></br>
					<div class="well">
						<h4><span class="glyphicon glyphicon-play"></span>&nbsp;November 07, 2015 - <a href="">Winter School on Computational Intelligence:</a></h4>
						<p>Dates: 21 - 23 Jan 2016 Venue: Bangladesh-Korea Information Access Center (IAC), Ground Floor, Dept of CSE, BUET Program Schedule Call for Participation: The three-day long Winter School on Computational Intelligence.....</p>
						<h4><span class="glyphicon glyphicon-play"></span>&nbsp;November 07, 2015 - <a href="">Winter School on Computational Intelligence:</a></h4>
						<p>Dates: 21 - 23 Jan 2016 Venue: Bangladesh-Korea Information Access Center (IAC), Ground Floor, Dept of CSE, BUET Program Schedule Call for Participation: The three-day long Winter School on Computational Intelligence.....</p>
						<p><a class="btn btn-primary btn-sm" href="#" role="button">Read all events >></a></p>
					</div>
			 	</div>
			 	<div class="header">
					<h3><span class="glyphicon glyphicon-th-large"></span>&nbsp;Others</h3></br>
					<div class="well">
						<h4><span class="glyphicon glyphicon-play"></span>&nbsp;Prospects of CSE:</h4>
						<p>This document was prepared as a leaflet for the seminar 'Prospects of CSE' organized by Department of CSE, BUET and CSEBUET 99 Alumni Association. The seminar was held on November 26, 2005 at the BUET Central Auditorium.e</p>
						<p><a class="btn btn-primary btn-sm" href="#" role="button">Learn more >></a></p>
					</div>
				 </div>
			</div><!-- col 6 start-->
			<div class="col-md-4">
				    <div class="header">
						<h3><span class="glyphicon glyphicon-blackboard"></span>&nbsp;Reserch Work</h3></br>
				    	<div class="well">
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
						</div>
				    </div>
				    <div class="header">
						<h3><span class="glyphicon glyphicon-flash"></span>&nbsp;CSE News</h3></br>
				    	<div class="well">
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><span class="glyphicon glyphicon-play"></span>&nbsp;<a href="">Graduate research work of CSE faculty appears at Swarm and Evolutionary Computation.</a></p>
							<p><a class="btn btn-primary btn-sm" href="#" role="button">Read more >></a></p>
						</div>
				    </div>

			</div>
		</div>
		<!-- Main body end-->
		  <!-- footer area-->
		 <nav class="navbar navbar-inverse navbar-fixed-bottom">
		  <div class="container-fluid">
		    <div class="col-md-8">
		     		<p class="footer-paragraph">&copy; Department of Computer Science Achademic buliding, Gazipur, Bangladesh. The Department is part of the Faculty of Natural Science at the National University. No part or content of this website may be copied or reproduced without permission of the department authority. Contact driqbalcse@gmail.com with questions or comments on this page.  <a href="">[Development Credits]</a></p>
		    </div>
		    <div class="col-md-4">
			    <form class="navbar-form navbar-right" role="search">
			        <div class="form-group">
			          <input type="text" class="form-control" placeholder="Search">
			        </div>
			        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			     </form>
		    </div>
		    
		  </div>
		</nav>
		<!-- footer area End-->
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
	</body>
</html>